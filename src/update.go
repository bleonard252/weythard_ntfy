package main

import (
	"encoding/json"
	"net/http"
	"strings"

	"codeberg.org/bleonard252/weythard-ntfy/src/models"
)

func RunUpdate() {
	resp, err := http.Get("https://api.weather.gov/alerts/active?status=actual")
	if err != nil {
		println("Error encountered on update. Delaying next update", err)
		return
	}

	var data map[string]interface{}
	decodeErr := json.NewDecoder(resp.Body).Decode(&data)
	if decodeErr != nil {
		panic(decodeErr)
	}

	for _, alert := range data["features"].([]interface{}) {
		alertData := alert.(map[string]interface{})
		alertUpdate := models.AlertUpdateModel{
			Severity:    models.AlertSeverity(alertData["properties"].(map[string]interface{})["severity"].(string)),
			Urgency:     models.AlertUrgency(alertData["properties"].(map[string]interface{})["urgency"].(string)),
			Response:    models.AlertResponse(alertData["properties"].(map[string]interface{})["response"].(string)),
			Event:       alertData["properties"].(map[string]interface{})["event"].(string),
			Url:         alertData["properties"].(map[string]interface{})["@id"].(string),
			Description: alertData["properties"].(map[string]interface{})["description"].(string),
		}
		for _, zone := range alertData["properties"].(map[string]interface{})["geocode"].(map[string]interface{})["UGC"].([]interface{}) {
			zone, ok := zone.(string)
			if !ok {
				continue
			}
			if strings.Contains(zone, "NCZ") {
				PublishUpdate("US_"+zone, alertUpdate)
			}
		}
	}
}
