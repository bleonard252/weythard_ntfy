package main

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"

	"codeberg.org/bleonard252/weythard-ntfy/src/models"
)

// The `zone` field should be the part included in the topic URL,
// i.e. "US_SCZ109" in "WX_US_SCZ109_SEVERE". PublishUpdate will
// handle the rest.
func PublishUpdate(zone string, update models.AlertUpdateModel) {
	request, err := http.NewRequest("POST", "https://ntfy.sh/WX_CONTROL", nil)
	tags := []string{}
	if strings.Contains(zone, "US_") {
		tags = append(tags, "NWS") // this should probably be elsewhere
	}
	if err != nil {
		println(err)
		return
	}
	request.Header.Add("Title", update.Event)
	request.Body = io.NopCloser(bytes.NewBufferString(update.Description)) // for now. In due time a custom message will be generated.
	request.Header.Add("Click", update.Url)
	request.Header.Add("X-Cache", "no")
	if update.Severity == models.AlertSeverityUnknown {
		println("Unknown severity on alert %s", update.Url)
	}
	tags = append(tags, "severity-"+string(update.Severity))
	tags = append(tags, "urgency-"+string(update.Urgency))
	tags = append(tags, "response-"+string(update.Response))
	switch update.Urgency {
	case models.Immediate:
		request.Header.Add("Priority", "max")
		break
	case models.Expected:
		request.Header.Add("Priority", "high")
		break
	case models.Future:
		request.Header.Add("Priority", "low")
		break
	case models.Past:
		request.Header.Add("Priority", "min")
		break
	default:
		request.Header.Add("Priority", "default")
	}
	url, urlError := url.Parse(fmt.Sprintf("https://ntfy.sh/WX_%s_%s", zone, strings.ToUpper(string(update.Severity))))
	if urlError != nil {
		panic(urlError)
	}
	request.URL = url
	request.Header.Add("Tags", strings.Join(tags, ","))
	http.DefaultClient.Do(request)
	return
}
