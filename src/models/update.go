package models

type AlertUpdateModel struct {
	Severity AlertSeverity `json:"severity"`
	// i.e. Tornado Warning, Severe Thunderstorm Warning,
	// Special Weather Statement, Frost Advisory, etc.
	Event string `json:"event"`
	// The given description.
	// We can maybe pull out key words from this,
	// maybe including wind speeds?
	Description string `json:"description"`
	// How people should respond to the threat.
	// Since our notifications are custom-made,
	// we need this to format our instructions.
	Response AlertResponse `json:"response"`
	Url      string        `json:"url"`
	Urgency  AlertUrgency  `json:"urgency"`
}

type AlertSeverity string

const (
	AlertSeverityUnknown AlertSeverity = "Unknown"
	Minor                AlertSeverity = "Minor"
	Moderate             AlertSeverity = "Moderate"
	Severe               AlertSeverity = "Severe"
	Extreme              AlertSeverity = "Extreme"
)

type AlertResponse string

// https://docs.oasis-open.org/emergency/cap/v1.2/CAP-v1.2-os.html#:~:text=alert%20message%20(REQUIRED)-,responsetype,-cap.
const (
	Prepare              AlertResponse = "Prepare"
	Execute              AlertResponse = "Execute"
	Avoid                AlertResponse = "Avoid"
	Monitor              AlertResponse = "Monitor"
	AllClear             AlertResponse = "AllClear"
	AlertResponseNone    AlertResponse = "None"
	AlertResponseUnknown AlertResponse = "Unknown"
)

type AlertUrgency string

const (
	AlertUrgencyUnknown AlertUrgency = "Unknown"
	Immediate           AlertUrgency = "Immediate"
	Expected            AlertUrgency = "Expected"
	Future              AlertUrgency = "Future"
	Past                AlertUrgency = "Past"
)
